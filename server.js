var express = require('express'),
    orientdb = require('orientjs');

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = express();

var config = {
    rootPath: __dirname
}

//Definition anderer verwendeter Dateien 
require('./server/config/express')(app, config);
require('./server/controller/login')(app, config);
require('./server/controller/loginPattern')(app, config);
require('./server/controller/register')(app, config);
require('./server/controller/logout')(app, config);


//Hosten des Servers
var port = 3000;
app.listen(port);
console.log('Listening on port ' + port + '...');
