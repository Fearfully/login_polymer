var hasher = require('../hasher'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    orientdb = require('orientjs');


module.exports = function () {

    passport.use(new LocalStrategy(function (username, password, done) {
        var user = {};

        process.nextTick(function () {

            //CONNECTION TO ORIENT DB
            var serverConfig = {
                username: "root",
                password: "admin",
                host: "localhost",
                port: 2424
            };
            var server = orientdb(serverConfig);
            var db = server.use('Login');
            console.log("Opening database: " + db.name);

            // SEARCHING IN ORIENTDB FOR USER 
            db.query('select from LoginUsers where username=:username', {
                params: {
                    username: username
                },
                limit: 1
            }).then(function (results) {
                if (results[0] === undefined) {
                    console.log("invalid Credentials.");
                    db.close();
                    console.log("Database closed");
                    return done(null, false);

                } else {

                    var testHash = hasher.computeHash(password, results[0].salt);
                    if (testHash === results[0].passwordHash || testHash === results[0].patternHash) {
                        console.log("User found, logging in...");
                        db.close();
                        console.log("Database closed");
                        return done(null, results[0]);

                    } else {
                        console.log("invalid Credentials.....");
                        db.close();
                        console.log("Database closed");
                        return done(null, false);
                    }
                }
            })
        });
    }));
}
