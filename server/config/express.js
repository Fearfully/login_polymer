var express = require('express'),
    logger = require('morgan'),
    session = require('express-session'),
    routes = require('../../routes/index'),
    users = require('../../routes/users'),
    bodyParser = require('body-parser'),
    path = require('path');

module.exports = function (app, config) {

    app.set('views', path.join(config.rootPath + '/server/views'));
    app.set('view engine', 'ejs');
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(express.static(path.join(config.rootPath + '/public')));
    app.use(session({
        secret: 'anything',
        resave: true,
        saveUninitialized: true
    }));

    require('./passport')(app);

    app.use('/', routes);
    app.use('/users', users);
}
