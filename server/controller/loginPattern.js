var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    local = require('../config/strategies/local.strategy');

module.exports = function (app, config) {

    app.post('/loginPattern', passport.authenticate('local'), function (req, res) {
        res.send({valid:true});
    });

}
