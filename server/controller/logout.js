var LocalStrategy = require('passport-local').Strategy,
    local = require('../config/strategies/local.strategy');

module.exports = function (app, config) {

    app.get('/logout', function (req, res) {
        console.log("Logout initiated");
        req.logout();
        res.redirect('/');
    });
}
