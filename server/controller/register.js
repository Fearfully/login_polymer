var bodyParser = require('body-parser'),
    hasher = require("../config/hasher"),
    orientdb = require('orientjs');

module.exports = function (app, config) {

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.post('/register', function (req, res) {
        var serverConfig = {
            username: "root",
            password: "admin",
            host: "localhost",
            port: 2424
        };
        var server = orientdb(serverConfig);
        var db = server.use('Login');
        console.log("Opening database: " + db.name);


        var salt = hasher.createSalt();
        //var newUser = req.body;
        //AJAX sends a Query atm --> not secure? Change needed! but for Prototype its okay
        var newUser = req.query;

        var user = {};
        var query = {
            'username': newUser.username
        };
        console.log(db.name);

        db.query('select from LoginUsers where username=:username', {
            params: {
                username: newUser.username
            },
            limit: 1
        }).then(function (results) {
            if (results != "") {
                console.log("Username already taken!" + results + " <--");
                res.send({
                    taken: true
                });
                db.close();
                console.log("Database closed");
            } else {
                console.log("Username available ... Creating User");
                db.query('insert into LoginUsers (username, passwordHash, patternHash, salt) values (:username, :passwordHash, :patternHash, :salt)', {
                    params: {
                        username: newUser.username,
                        passwordHash: hasher.computeHash(req.query.password, salt),
                        patternHash: hasher.computeHash(req.query.pattern, salt),
                        salt: salt
                    }
                }).then(function (response) {
                    res.send({
                        taken: false
                    });
                    db.close();
                    console.log("Database closed");
                    // console.log(response);
                })
            }
        });
    });
}
