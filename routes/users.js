var express = require('express'),
    router = express.Router(),
    login = require('../server/controller/login');

router.get('/', function (req, res, next) {
    if (!req.user) {
        res.redirect('/');
    } else {
        res.render('users', {
            user: req.user
        });
    }
});

module.exports = router;
